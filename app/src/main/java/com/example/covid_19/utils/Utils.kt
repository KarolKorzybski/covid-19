package com.example.covid_19.utils

import android.annotation.SuppressLint
import com.github.mikephil.charting.utils.ColorTemplate
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    @SuppressLint("SimpleDateFormat")
    fun changeFormatDate(dateText : String?, patternIn : String = PATTER_REST, patternOut : String = PATTER_OUT) : String{
        if(dateText.isNullOrEmpty()) return ""
        val parser = SimpleDateFormat(patternIn)
        parser.timeZone = TimeZone.getTimeZone("UTC")
        val formatter = SimpleDateFormat(patternOut)
        val date = parser.parse(dateText)
        return if(date == null) dateText
        else formatter.format(date)
    }
    fun changeFormatDateToTime(dateText : String?, patternIn : String = PATTER_REST) : Long{
        if(dateText.isNullOrEmpty()) return 0
        val parser = SimpleDateFormat(patternIn)
        parser.timeZone = TimeZone.getTimeZone("UTC")
        val date = parser.parse(dateText)
        return date?.time ?: 0
    }
    companion object{
        const val PATTER_REST = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        const val PATTER_OUT = "dd.MM.yyyy HH:mm"
        const val PATTER_DAY_OUT = "dd MM"
    }

    val colorsChart = intArrayOf(
        ColorTemplate.VORDIPLOM_COLORS[0],
        ColorTemplate.VORDIPLOM_COLORS[1],
        ColorTemplate.VORDIPLOM_COLORS[2]
    )

    val labelsChart = arrayOf(
        "confirmed",
        "active",
        "death"
    )

}