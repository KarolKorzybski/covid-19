package com.example.covid_19.data

import androidx.room.TypeConverter
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.Global
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
    @TypeConverter
    fun listToJson(value: List<CountriesCovidItem>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<CountriesCovidItem>::class.java).toList()
    @TypeConverter
    fun globalToJson(value: Global?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToGlobal(value: String) = Gson().fromJson(value, Global::class.java)
}