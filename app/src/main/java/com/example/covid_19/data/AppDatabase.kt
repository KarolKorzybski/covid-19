package com.example.covid_19.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.covid_19.data.dao.CountriesAllItemDao
import com.example.covid_19.data.dao.CountriesDayDao
import com.example.covid_19.data.dao.GetSummaryDao
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.CountryDay
import com.example.covid_19.rest.response.GetSummary

@Database(
    entities = [
        CountriesCovidItem::class,
        CountryDay::class,
        GetSummary::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun coutriesAllItemDao(): CountriesAllItemDao
    abstract fun getSummaryDao(): GetSummaryDao
    abstract fun getCountriesDayDao(): CountriesDayDao
}