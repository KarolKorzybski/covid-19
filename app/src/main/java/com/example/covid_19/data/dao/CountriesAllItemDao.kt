package com.example.covid_19.data.dao

import androidx.room.*
import com.example.covid_19.model.CountriesCovidItem
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface CountriesAllItemDao {


    @Query("SELECT * FROM COUNTRIES_ALL_ITEM")
    fun getAll(): Maybe<List<CountriesCovidItem>>

    //
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setAllCountries(hits: List<CountriesCovidItem>): Completable
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setCountriesItem(countriesItem: CountriesCovidItem): Completable

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAllCountries(results: List<CountriesCovidItem>): Completable

    @Query("DELETE FROM COUNTRIES_ALL_ITEM")
    fun deleteAll(): Completable
}