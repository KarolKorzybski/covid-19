package com.example.covid_19.data.dao

import androidx.room.*
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.CountryDay
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface CountriesDayDao {
    @Query("SELECT * FROM COUNTRIES_DAYS")
    fun getAllCountryDays(): Maybe<List<CountryDay>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setAllCountries(hits: List<CountryDay>): Completable
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setCountry(countriesItem: CountryDay): Completable

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAllCountries(results: List<CountryDay>): Completable

    @Query("DELETE FROM COUNTRIES_DAYS")
    fun deleteAll(): Completable
}