package com.example.covid_19.data.dao

import androidx.room.*
import com.example.covid_19.rest.response.GetSummary
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface GetSummaryDao {


    @Query("SELECT * FROM GET_SUMMARY")
    fun getAll(): Single<GetSummary>

    //
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setSummary(getSummary: GetSummary): Completable

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateSummary(results: GetSummary): Completable

    @Query("DELETE FROM GET_SUMMARY")
    fun deleteAll(): Completable
}