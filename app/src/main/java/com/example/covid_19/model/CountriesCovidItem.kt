package com.example.covid_19.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "COUNTRIES_ALL_ITEM")
data class CountriesCovidItem(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @SerializedName("Country") val country: String?,
    @SerializedName("CountryCode") val countryCode: String?,
    @SerializedName("Date") val date: String?,
    @SerializedName("NewConfirmed") val newConfirmed: Int?,
    @SerializedName("NewDeaths") val newDeaths: Int?,
    @SerializedName("NewRecovered") val newRecovered: Int?,
    @SerializedName("Slug") val slug: String?,
    @SerializedName("TotalConfirmed") val totalConfirmed: Int?,
    @SerializedName("TotalDeaths") val totalDeaths: Int?,
    @SerializedName("TotalRecovered") val totalRecovered: Int?
) : Serializable