package com.example.covid_19.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Global(
    @SerializedName("NewConfirmed") val newConfirmed: Int?,
    @SerializedName("NewDeaths") val newDeaths: Int?,
    @SerializedName("NewRecovered") val newRecovered: Int?,
    @SerializedName("TotalConfirmed") val totalConfirmed: Int?,
    @SerializedName("TotalDeaths") val totalDeaths: Int?,
    @SerializedName("TotalRecovered") val totalRecovered: Int?
) : Serializable