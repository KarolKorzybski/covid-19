package com.example.covid_19.viewmodel

import androidx.lifecycle.ViewModel
import com.example.covid_19.data.dao.GetSummaryDao
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.CountryDay
import com.example.covid_19.model.Global
import com.example.covid_19.rest.CovidApi
import com.example.covid_19.rest.response.GetSummary
import com.example.covid_19.utils.SingleLiveEvent
import com.example.covid_19.utils.Utils
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.osmdroid.util.GeoPoint
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class MapsViewModel() : ViewModel() {
    fun createGetSummary(countriesCovidItemList: ArrayList<CountriesCovidItem>, date: String?, global: Global): GetSummary {
        return GetSummary(countriesCovidItemList, date, global)
    }

    @Inject
    constructor(covidApi: CovidApi, getSummaryDao: GetSummaryDao) : this(){
        this.covidApi = covidApi
        this.getSummaryDao = getSummaryDao
        utils = Utils()
    }
    constructor(countryDay: CountryDay) : this(){
        this.countryDay = countryDay
    }
    private lateinit var utils: Utils
    var listCountryDay = SingleLiveEvent<List<CountryDay>?>()
    var countryDay : CountryDay?=null
    lateinit var covidApi: CovidApi
    lateinit var getSummaryDao: GetSummaryDao

    fun getAllDaysAboutCountry(name : String) {
        val disposable = covidApi
            .getAllDaysAboutCountry(name)
            .subscribeOn(Schedulers.newThread())
//            ?.repeatWhen { completed -> completed.delay(1, TimeUnit.SECONDS) }
//            ?.retryWhen { error -> error.delay(1, TimeUnit.SECONDS) }
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    listCountryDay.value = it
                },      // onNext
                { error ->
                    println(error.message)
                    listCountryDay.value = null
                },         // onError
                {
                    println("Completed")
                }                       // onComplete
            )
    }

    fun createGeoPoint(list: List<CountryDay>): GeoPoint? {
        val lat = list.first().lat.toDoubleOrNull()
        val lon = list.first().lon.toDoubleOrNull()
        return if(lat!=null && lon != null) GeoPoint(lat, lon)
        else null
    }

    fun createDataList(list: List<CountryDay>): ArrayList<ArrayList<Int>> {
        val dataList = ArrayList<ArrayList<Int>>()
        dataList.add(list.map { it.confirmed } as ArrayList<Int>)
        dataList.add(list.map { it.active } as ArrayList<Int>)
        dataList.add(list.map { it.deaths } as ArrayList<Int>)
        return dataList
    }

    private fun createListForLine(listLine: ArrayList<Int>, list: List<CountryDay>): ArrayList<Entry> {
        val values = ArrayList<Entry>()
        val firstDay = list.map { it.date }.first()
        val day = TimeUnit.MILLISECONDS.toDays(utils.changeFormatDateToTime(firstDay))
        for ((i,line) in listLine.withIndex()){
            values.add(Entry(day.plus(i).toFloat(),line.toFloat()))
        }
        return values
    }

    private fun createLineDataSet(values: ArrayList<Entry>, j: Int): LineDataSet {
        val lineDataSet = LineDataSet(values, "DataSet " + (j + 1))
        lineDataSet.lineWidth = 2.5f
        lineDataSet.circleRadius = 4f
        lineDataSet.color = utils.colorsChart[j]
        lineDataSet.label = utils.labelsChart[j]
        lineDataSet.setCircleColor(lineDataSet.color)
        return lineDataSet
    }

    fun createDataSets(list: List<CountryDay>): MutableList<ILineDataSet>? {
        val dataList = createDataList(list)
        val dataSets = ArrayList<ILineDataSet>()
        for ((j,listLine) in dataList.withIndex()){
            val values = createListForLine(listLine,list)
            dataSets.add(createLineDataSet(values,j))
        }
        return dataSets
    }
}