package com.example.covid_19.viewmodel

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.covid_19.utils.Utils
import com.example.covid_19.data.dao.GetSummaryDao
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.Global
import com.example.covid_19.rest.CovidApi
import com.example.covid_19.rest.response.GetSummary
import com.example.covid_19.utils.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class MainViewModel() : ViewModel() {
    @Inject
    constructor(covidApi: CovidApi, getSummaryDao: GetSummaryDao) : this(){
        this.covidApi = covidApi
        this.getSummaryDao = getSummaryDao
        utils = Utils()
    }
    var getSummary : GetSummary? = null
    val countriesCovidItemLiveData = SingleLiveEvent<CountriesCovidItem>()
    var oldValue : CharSequence = ""
    var loadedVisibility = ObservableField(true)
    lateinit var covidApi: CovidApi
    lateinit var getSummaryDao: GetSummaryDao
    private lateinit var utils: Utils
    val getSummaryWithDatabase = SingleLiveEvent<GetSummary?>()
    val getSummaryWithRest = SingleLiveEvent<GetSummary>()
    val loaded = SingleLiveEvent<Boolean>()
    var global : Global?=null
    var date : String?=null
    var countriesCovidItem : CountriesCovidItem?=null
    var type : Int? = null
    fun getSummaryCovid() {
        val disposable = covidApi
            .getSummary()
            .subscribeOn(Schedulers.newThread())
//            ?.repeatWhen { completed -> completed.delay(1, TimeUnit.SECONDS) }
//            ?.retryWhen { error -> error.delay(1, TimeUnit.SECONDS) }
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    getSummaryWithRest.value = it
                    loaded.value = false
                },      // onNext
                { error ->
                    println(error.message)
                    loaded.value = false
                },         // onError
                {
                    println("Completed")
                }                       // onComplete
            )
    }
    fun deleteAndSaveSummaryToDatabase(getSummary: GetSummary){
        deleteAllWithDatabase(getSummary)
    }
    fun getSummaryWithDatabase(){
        getSummaryDao
            .getAll()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableSingleObserver<GetSummary>() {
                override fun onSuccess(getSummary: GetSummary) {
                    getSummaryWithDatabase.value = getSummary
                    loaded.value = false
                }

                override fun onError(e: Throwable) {
                    getSummaryWithDatabase.value = null
                    loaded.value = false
                }
            })
    }
    private fun saveToDatabase(getSummary: GetSummary){
        getSummaryDao
            .setSummary(getSummary)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableCompletableObserver() {
                override fun onComplete() {
                    Log.d("","")
                }

                override fun onError(e: Throwable) {
                    Log.d("","")
                }

            })
    }
    private fun deleteAllWithDatabase(getSummary: GetSummary){
        getSummaryDao
            .deleteAll()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableCompletableObserver() {
                override fun onComplete() {
                    saveToDatabase(getSummary)
                }

                override fun onError(e: Throwable) {
                    Log.d("","")
                }
            })
    }
    fun createAdapter(getSummary: GetSummary?, utils: Utils = this.utils) : ArrayList<MainViewModel>{
        if(getSummary==null) return ArrayList()
        val adapter = ArrayList<MainViewModel>()
        adapter.add(MainViewModel(getSummary.global, utils.changeFormatDate(getSummary.date), TYPE_GLOBAL))
        for (countriesCovidItem in getSummary.countries){
            adapter.add(MainViewModel(countriesCovidItem, TYPE_DATA))
        }
        return adapter
    }

    constructor(global: Global?, date : String?, type : Int) : this() {
        this.global = global
        this.date = date
        this.type = type
    }

    constructor(countriesCovidItem: CountriesCovidItem, type : Int) : this() {
        this.countriesCovidItem = countriesCovidItem
        this.type = type
    }
    companion object{
        const val TYPE_GLOBAL = 1
        const val TYPE_DATA = 0
    }
    fun isNullSummary(getSummary : GetSummary?) : Boolean{
        if(getSummary == null) return true
        return getSummary.countries.isNullOrEmpty() || getSummary.date.isNullOrEmpty()
    }

    fun performFiltering(value: CharSequence, allElements: List<MainViewModel>): ArrayList<MainViewModel> {
        if(value.isEmpty()) return allElements as ArrayList<MainViewModel>
        oldValue = value
        val filterList = HashSet<MainViewModel>()
        val filterPattern = value.toString().toLowerCase().trim()
        for (item in allElements){
            if(oldValue != value) break
            if(item.countriesCovidItem?.country?.toLowerCase()?.contains(filterPattern) == true){
                filterList.add(item)
            }
        }
        return ArrayList(filterList)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MainViewModel

        if (countriesCovidItem != other.countriesCovidItem) return false

        return true
    }

    override fun hashCode(): Int {
        return countriesCovidItem?.hashCode() ?: 0
    }

}