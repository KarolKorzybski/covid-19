package com.example.covid_19.dagger

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

open class MyApplication: MultiDexApplication(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext

        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()

        appComponent?.inject(this)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
        MultiDex.install(this)
    }

    companion object {
        var appContext: Context? = null
            private set
    }
}
