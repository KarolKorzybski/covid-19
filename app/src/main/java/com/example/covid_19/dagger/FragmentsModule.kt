package com.example.covid_19.dagger

import com.example.covid_19.view.fragment.MainFragment
import com.example.covid_19.view.fragment.MapsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun contributesMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributesMapsFragment(): MapsFragment
}