package com.example.covid_19.dagger

import android.content.Context
import androidx.room.Room
import com.example.covid_19.data.AppDatabase
import com.example.covid_19.data.dao.CountriesAllItemDao
import com.example.covid_19.data.dao.CountriesDayDao
import com.example.covid_19.data.dao.GetSummaryDao
import com.example.covid_19.other.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDb(context: Context): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
//                .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideCountriesAllItemDao(db: AppDatabase): CountriesAllItemDao {
        return db.coutriesAllItemDao()
    }

    @Singleton
    @Provides
    fun provideGetSummaryDao(db: AppDatabase): GetSummaryDao {
        return db.getSummaryDao()
    }

    @Singleton
    @Provides
    fun provideCountriesDayDao(db: AppDatabase): CountriesDayDao {
        return db.getCountriesDayDao()
    }

}