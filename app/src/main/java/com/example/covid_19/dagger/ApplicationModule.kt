package com.example.covid_19.dagger

import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule

@Module(includes = [ViewModelModule::class,
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class])
public class ApplicationModule {

}