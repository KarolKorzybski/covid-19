package com.example.covid_19.callback

import com.example.covid_19.model.CountriesCovidItem

interface AdapterCountriesDataCallback {
    fun click(countriesCovidItem: CountriesCovidItem)
}