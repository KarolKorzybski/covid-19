package com.example.covid_19.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.covid_19.R
import com.example.covid_19.callback.MainFragmentCallback
import com.example.covid_19.dagger.ViewModelFactory
import com.example.covid_19.databinding.FragmentMainBinding
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.view.adapter.CountriesAllDataAdapter
import com.example.covid_19.viewmodel.MainViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class MainFragment : Fragment(), MainFragmentCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private var adapter: CountriesAllDataAdapter? = null

    private lateinit var binding: FragmentMainBinding
    private lateinit var viewModel: MainViewModel

    private fun init() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        viewModel = ViewModelProvider(this,viewModelFactory).get(MainViewModel::class.java)
        binding.lifecycleOwner = this
        binding.callback = this
        binding.viewmodel = viewModel
    }
    private fun observable(){
        viewModel.getSummaryWithDatabase()
        viewModel.getSummaryWithDatabase.observe(viewLifecycleOwner, Observer {
            if(viewModel.isNullSummary(it) && viewModel.isNullSummary(viewModel.getSummaryWithRest.value)){
                viewModel.getSummaryCovid()
            }
            else {
                viewModel.getSummary = it
                setAdapter(viewModel.createAdapter(it))
            }
        })
        viewModel.getSummaryWithRest.observe(viewLifecycleOwner, Observer {
            if(viewModel.isNullSummary(it).not()) {
                viewModel.getSummary = it
                viewModel.deleteAndSaveSummaryToDatabase(it)
                setAdapter(viewModel.createAdapter(it))
            }
        })
        viewModel.loaded.observe(viewLifecycleOwner, Observer {
            viewModel.loadedVisibility.set(it)
        })
        viewModel.countriesCovidItemLiveData.observe(viewLifecycleOwner, Observer {
            if(viewModel.getSummary!=null) gotoMapsFragment(it)
        })
    }

    private fun gotoMapsFragment(it: CountriesCovidItem) {
        val neFragment: Fragment = MapsFragment.newInstance(viewModel.getSummary!!,it)
        fragmentManager!!.beginTransaction().replace(R.id.frameLayout, neFragment)
            .addToBackStack("frags").commit()
    }

    private fun setAdapter(results: List<MainViewModel>) {
        adapter = CountriesAllDataAdapter(ArrayList(results),ArrayList(results), viewModel)
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.adapter = adapter
        adapter?.notifyDataSetChanged()
        layoutAnimation(binding.recyclerView)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_main,container,false)
        init()
        observable()
        initAndSetListenerSearchView()
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = MainFragment()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun click() {
        viewModel.loaded.value = true
        viewModel.getSummaryCovid()
    }

    private fun initAndSetListenerSearchView() {
        with(binding.searchView){
            isActivated = true
            isIconified = false
            clearFocus()
            onActionViewExpanded()
            setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    return false
                }
                override fun onQueryTextChange(text: String): Boolean {
                    adapter?.filter?.filter(text)
                    return false
                }
            })
            setOnCloseListener {
                adapter?.filter?.filter("")
                false
            }
        }
    }

    private fun layoutAnimation(recyclerView: RecyclerView){
            recyclerView.layoutAnimation = AnimationUtils.loadLayoutAnimation(recyclerView.context, R.anim.layout_down_to_up)
            recyclerView.adapter?.notifyDataSetChanged()
            recyclerView.scheduleLayoutAnimation()
    }
}
