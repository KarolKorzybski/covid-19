package com.example.covid_19.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.covid_19.R
import com.example.covid_19.view.fragment.MainFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startMainFragment()
    }
    private fun startMainFragment(){
        val newFragment: Fragment = MainFragment.newInstance()
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, newFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    fun replaceFragment(newFragment: Fragment){
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, newFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
