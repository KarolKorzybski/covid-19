package com.example.covid_19.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.covid_19.R
import com.example.covid_19.callback.AdapterCountriesDataCallback
import com.example.covid_19.databinding.AdapterCountriesAllDataRow
import com.example.covid_19.databinding.AdapterGlobalDataRow
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.viewmodel.MainViewModel

class CountriesAllDataAdapter(private var arrayList: ArrayList<MainViewModel>,private var allElements: List<MainViewModel>,
                     private var viewModel: MainViewModel
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder >(), Filterable, AdapterCountriesDataCallback {
    companion object{
        const val TYPE_GLOBAL = MainViewModel.TYPE_GLOBAL
        const val TYPE_DATA = MainViewModel.TYPE_DATA
    }

    private var layoutInflater: LayoutInflater? = null

    override fun getItemViewType(position: Int): Int {
        return when(arrayList[position].type) {
            TYPE_DATA -> TYPE_DATA
            TYPE_GLOBAL -> TYPE_GLOBAL
            else -> super.getItemViewType(position)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.context)
        }
        when(i){
            TYPE_DATA ->{
                val adapterRow : AdapterCountriesAllDataRow = DataBindingUtil.inflate<ViewDataBinding>(
                    layoutInflater!!,
                    R.layout.row_countries_all_data,
                    viewGroup,
                    false
                ) as AdapterCountriesAllDataRow
                adapterRow.callback = this
                return CountriesAllDataView(adapterRow)
            }
            else ->{
                val adapterRow : AdapterGlobalDataRow = DataBindingUtil.inflate<ViewDataBinding>(
                    layoutInflater!!,
                    R.layout.row_global_data,
                    viewGroup,
                    false
                ) as AdapterGlobalDataRow
                return GlobalView(adapterRow)
            }
        }
    }


    override fun getItemCount(): Int {
        return if (arrayList.isNullOrEmpty()) 0 else arrayList.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when(arrayList[position].type){
            TYPE_DATA ->{
                val holder = viewHolder as CountriesAllDataView
                val result = arrayList[position]
                holder.bind(result)
            }
            TYPE_GLOBAL ->{
                val holder = viewHolder as GlobalView
                val result = arrayList[position]
                holder.bind(result)
            }
        }
    }

    class CountriesAllDataView(private var adapterRow: AdapterCountriesAllDataRow) :
        RecyclerView.ViewHolder(adapterRow.root) {

        fun bind(result: MainViewModel) {
            this.adapterRow.item = result.countriesCovidItem
            adapterRow.executePendingBindings()
        }

    }

    class GlobalView(private var adapterRow: AdapterGlobalDataRow) :
        RecyclerView.ViewHolder(adapterRow.root) {

        fun bind(result: MainViewModel) {
            this.adapterRow.item = result.global
            this.adapterRow.date = result.date
            adapterRow.executePendingBindings()
        }

    }

    override fun getFilter(): Filter {
        return object: Filter(){
            override fun performFiltering(value: CharSequence): FilterResults {
                val results = FilterResults()
                results.values = viewModel.performFiltering(value,allElements)
                return results
            }

            override fun publishResults(p0: CharSequence?, results: FilterResults) {
                arrayList.clear()
                arrayList.addAll(results.values as ArrayList<MainViewModel>)
                notifyDataSetChanged()
            }

        }
    }

    override fun click(countriesCovidItem: CountriesCovidItem) {
        viewModel.countriesCovidItemLiveData.value = countriesCovidItem
    }
}
