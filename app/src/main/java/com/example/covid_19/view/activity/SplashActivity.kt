package com.example.covid_19.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.covid_19.R
import com.example.covid_19.callback.SplashActivityCallback
import com.example.covid_19.databinding.SplashActivityBinding


class SplashActivity : AppCompatActivity(), SplashActivityCallback {
    private lateinit var binding: SplashActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        binding.callback = this
        setAnimation()
    }

    override fun clickStart() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    private fun setAnimation(){
        binding.imageViewReport.animation = AnimationUtils.loadAnimation(this,R.anim.from_top)
        binding.imageViewStart.animation = AnimationUtils.loadAnimation(this,R.anim.from_bottom)
    }
}