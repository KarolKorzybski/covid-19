package com.example.covid_19.view.fragment

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.covid_19.R
import com.example.covid_19.callback.ChartCallback
import com.example.covid_19.dagger.ViewModelFactory
import com.example.covid_19.databinding.FragmentMapsBinding
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.CountryDay
import com.example.covid_19.model.Global
import com.example.covid_19.rest.response.GetSummary
import com.example.covid_19.viewmodel.MapsViewModel
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.Legend.LegendForm
import com.github.mikephil.charting.components.YAxis.AxisDependency
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.MPPointF
import com.mancj.slideup.SlideUp
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.daily_country_chart.view.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList

class MapsFragment : Fragment(), ChartCallback,
    OnChartValueSelectedListener {
    lateinit var getSummary: GetSummary
    lateinit var countriesCovidItem: CountriesCovidItem

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: FragmentMapsBinding
    private lateinit var viewModel: MapsViewModel
    private lateinit var slideUp: SlideUp
    private lateinit var chart : LineChart

    private fun getSerializable(){
        arguments?.let {
            countriesCovidItem = it.getSerializable(ARG_COVID_ITEM) as CountriesCovidItem
            getSummary = viewModel.createGetSummary(
                it.getSerializable(ARG_GET_SUMMARY_LIST) as ArrayList<CountriesCovidItem>,
                it.getString(ARG_GET_SUMMARY_DATE),
                it.getSerializable(ARG_GET_SUMMARY_GLOBAL) as Global)
        }
        activity?.title = countriesCovidItem.country
        initMaps()
    }
    private fun init() {
        viewModel = ViewModelProvider(this,viewModelFactory).get(MapsViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        binding.callback = this
    }
    private fun observable(){
        viewModel.listCountryDay.observe(viewLifecycleOwner, Observer{
            if(!it.isNullOrEmpty()){
                val geoPoint = viewModel.createGeoPoint(it)
                if(geoPoint!=null) setMarkerOnMaps(geoPoint)
                initSlide()
                setPointsToChart(it)
            }
        })
    }

    private fun initSlide(){
        slideUp = SlideUp(binding.linearLayout.slideView)
        slideUp.animateOut()
        slideUp.hideImmediately()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_maps,container,false)
        init()
        initSlide()
        getSerializable()
        observable()
        sendRequestGetAllDaysByCountry()
        initChart()
        setLegend()
        return binding.root
    }

    private fun initMaps() {
        Configuration.getInstance().load(activity, PreferenceManager.getDefaultSharedPreferences(activity))
        binding.map.setTileSource(TileSourceFactory.MAPNIK)

        requestPermissionsIfNecessary(arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ))
    }

    private fun sendRequestGetAllDaysByCountry(){
        viewModel.getAllDaysAboutCountry(countriesCovidItem.slug?:"")
    }

    private fun setMarkerOnMaps(geoPoint: GeoPoint) {

        val marker = Marker(binding.map)
        marker.icon = this.resources.getDrawable(R.drawable.ic_virus)
        marker.position = geoPoint
        binding.map.post {
            binding.map.overlays.add(marker)
            binding.map.controller.animateTo(marker.position,9.0,2)
            binding.map.invalidate()
        }
        marker.setOnMarkerClickListener { _, _ ->
            Log.d("","")
            slideUp.animateIn()
            false
        }
    }

    companion object {
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 1
        private const val ARG_GET_SUMMARY_LIST = "GetSummaryList"
        private const val ARG_GET_SUMMARY_DATE = "GetSummaryDate"
        private const val ARG_GET_SUMMARY_GLOBAL = "GetSummaryGlobal"
        private const val ARG_COVID_ITEM = "CountriesCovidItem"

        @JvmStatic
        fun newInstance(getSummary: GetSummary, covidItem: CountriesCovidItem) =
            MapsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_GET_SUMMARY_LIST, getSummary.countries as ArrayList<CountriesCovidItem>)
                    putString(ARG_GET_SUMMARY_DATE, getSummary.date )
                    putSerializable(ARG_GET_SUMMARY_GLOBAL, getSummary.global)
                    putSerializable(ARG_COVID_ITEM, covidItem)
                }
            }
    }
    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    override fun onResume() {
        super.onResume()
        binding.map.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.map.onPause()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        val permissionsToRequest = ArrayList<String>()
        for (element in grantResults) {
            permissionsToRequest.add(permissions[element])
        }
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(
                activity!!,
                permissionsToRequest.toTypedArray(),
                REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    private fun requestPermissionsIfNecessary(permissions: Array<String>) {
        val permissionsToRequest = ArrayList<String>()
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(activity!!, permission)
                != PackageManager.PERMISSION_GRANTED) { // Permission is not granted
                permissionsToRequest.add(permission)
            }
        }
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(
                activity!!,
                permissionsToRequest.toTypedArray(),
                REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }
    private fun initChart(){
        chart = binding.linearLayout.chart
        chart.setDrawGridBackground(true)
        chart.description.isEnabled = true
        chart.description.text = "Covid-19 in days"
        chart.setDrawBorders(true)
        chart.axisLeft.isEnabled = false
        chart.axisRight.setDrawAxisLine(true)
        chart.axisRight.setDrawGridLines(true)
        chart.xAxis.setDrawAxisLine(true)
        chart.xAxis.setDrawGridLines(true)
        chart.setTouchEnabled(true)
        chart.isDragEnabled = true
        chart.setScaleEnabled(true)
        chart.setPinchZoom(false)
        chart.setOnChartValueSelectedListener(this)

        chart.xAxis.valueFormatter = object : ValueFormatter() {
            private val mFormat =
                SimpleDateFormat("dd MM", Locale.ENGLISH)

            override fun getFormattedValue(value: Float): String {
                val millis =
                    TimeUnit.DAYS.toMillis(value.toLong())
                return mFormat.format(Date(millis))
            }
        }

    }
    private fun setLegend(){
        val legend: Legend = chart.legend
        legend.isEnabled = true
        legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        legend.setDrawInside(false)
        legend.form = LegendForm.LINE
        chart.invalidate()
    }

    private fun setPointsToChart(list: List<CountryDay>) {
        chart.data = LineData(viewModel.createDataSets(list))
        chart.invalidate()
    }

    override fun clickExit() {
        slideUp.animateOut()
    }

    override fun onNothingSelected() {
    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {
        Log.i(
            "VAL SELECTED",
            "Value: " + e?.y + ", xIndex: " + e?.x
                    + ", DataSet index: " + h?.dataSetIndex
        )
        val position = chart.getPosition(e, AxisDependency.LEFT)
        MPPointF.recycleInstance(position)
    }

}