package com.example.covid_19.other

import com.example.covid_19.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class AddHeaderInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder: Request.Builder = chain.request().newBuilder()
        builder.addHeader(BuildConfig.HOST_KEY, BuildConfig.HOST_VALUE)
        builder.addHeader(BuildConfig.KEY_KEY, BuildConfig.KEY_VALUE)
        builder.addHeader(BuildConfig.USE_KEY, BuildConfig.USE_VALUE)
        return chain.proceed(builder.build())
    }
}