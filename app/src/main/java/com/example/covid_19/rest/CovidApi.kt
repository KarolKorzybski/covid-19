package com.example.covid_19.rest

import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.CountryDay
import com.example.covid_19.rest.response.GetSummary
import retrofit2.http.GET
import io.reactivex.Observable
import retrofit2.http.Path


interface CovidApi {

    @GET("all")
    fun getAllCountries(): Observable<List<CountriesCovidItem>>

    @GET("dayone/country/{countryName}")
    fun getAllDaysAboutCountry(@Path("countryName") country : String): Observable<List<CountryDay>>

    @GET("summary")
    fun getSummary(): Observable<GetSummary>
}