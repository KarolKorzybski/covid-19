package com.example.covid_19.rest.response

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.Global
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "GET_SUMMARY")
class GetSummary(@SerializedName("Countries") var countries: List<CountriesCovidItem>,
                 @SerializedName("Date") var date: String?,
                 @SerializedName("Global") var global: Global
) : Serializable{
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}