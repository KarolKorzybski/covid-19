package com.example.covid_19.utils

import junit.framework.Assert.assertEquals
import org.junit.Test

class UtilsTest {
    private val utils = Utils()
    @Test
    fun testChangeFormatDate() {
        //given
        val dateText = "2020-06-25T05:26:47Z"

        //when
        val formatDate = utils.changeFormatDate(dateText)

        //then
        val output = "25.06.2020 07:26"
        assertEquals(output, formatDate)
    }
    @Test
    fun testChangeFormatDateEmpty() {
        //given
        val dateText = ""

        //when
        val formatDate = utils.changeFormatDate(dateText)

        //then
        assertEquals("", formatDate)
    }
    @Test
    fun testChangeFormatDateToTime() {
        //given
        val firstDay = "2020-02-24T00:00:00Z"

        //when
        val formatDate = utils.changeFormatDateToTime(firstDay)

        //then
        assertEquals(1582502400000, formatDate)
    }

    @Test
    fun testChangeFormatDateToTimeEmpty() {
        //given
        val firstDay = ""

        //when
        val formatDate = utils.changeFormatDateToTime(firstDay)

        //then
        assertEquals(0, formatDate)
    }
}
