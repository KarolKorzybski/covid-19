package com.example.covid_19.viewmodel

import com.example.covid_19.model.CountriesCovidItem
import com.example.covid_19.model.Global
import com.example.covid_19.rest.response.GetSummary
import com.example.covid_19.utils.Utils
import junit.framework.Assert.assertEquals
import org.junit.Test


class MainViewModelTest {
    private val viewModel: MainViewModel = MainViewModel()
    private val utils = Utils()

    @Test
    fun createAdapterEmpty() {
        //given
        val getSummary: GetSummary? = null

        //when
        val listEmpty = viewModel.createAdapter(getSummary, utils)

        //then
        assertEquals(0, listEmpty.size)
    }

    @Test
    fun createAdapter() {
        //given
        val getSummary: GetSummary = getSummary()

        //when
        val list = viewModel.createAdapter(getSummary, utils)

        //then
        assertEquals(2, list.size)
    }

    private fun getSummary(): GetSummary {
        return GetSummary(
            arrayListOf(
                CountriesCovidItem(
                    0,
                    "Afghanistan",
                    "AF",
                    "2020-06-25T05:26:47Z",
                    324,
                    20,
                    419,
                    "afghanistan",
                    29481,
                    618,
                    9260
                )
            ),
            "2020-06-25T05:26:47Z",
            Global(
                165172,
                5431,
                104369,
                9366512,
                486079,
                4630051
            )
        )
    }
}