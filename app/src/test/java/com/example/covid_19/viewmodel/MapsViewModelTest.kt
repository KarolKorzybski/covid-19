package com.example.covid_19.viewmodel

import com.example.covid_19.model.CountryDay
import com.example.covid_19.utils.Utils
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.Test


class MapsViewModelTest {
    private val viewModel = MapsViewModel()
    private val utils = Utils()

    @Test
    fun createDataList(){
        //given
        val list = getListCountryDays()
        val listComparison = arrayListOf(arrayListOf(2), arrayListOf(2), arrayListOf(0))

        //when
        val listOutput = viewModel.createDataList(list)

        //then
        assertThat(listComparison, `is`(equalTo(listOutput)))
    }

    @Test
    fun createGeoPointNull() {
        //given
        val list = getListCountryDaysEmpty()

        //when
        val listOutput = viewModel.createGeoPoint(list)

        //then
        assertEquals(null, listOutput)
    }

    @Test
    fun createGeoPoint() {
        //given
        val list = getListCountryDays()

        //when
        val geoPoint = viewModel.createGeoPoint(list)

        //then
        assertNotNull(geoPoint)
        assertEquals(41.15, geoPoint?.latitude)
        assertEquals(20.17, geoPoint?.longitude)
    }

    private fun getListCountryDays(): ArrayList<CountryDay> {
        return arrayListOf(
            CountryDay(
                id = 0,
                active = 2,
                city = "",
                cityCode = "",
                confirmed = 2,
                country = "Albania",
                countryCode = "AL",
                date = "2020-03-09T00:00:00Z",
                deaths = 0,
                lat = "41.15",
                lon = "20.17",
                province = "",
                recovered = 0
            )
        )
    }

    private fun getListCountryDaysEmpty(): ArrayList<CountryDay> {
        return arrayListOf(
            CountryDay(
                id = 0,
                active = 0,
                city = "",
                cityCode = "",
                confirmed = 0,
                country = "",
                countryCode = "",
                date = "",
                deaths = 0,
                lat = "",
                lon = "",
                province = "",
                recovered = 0
            )
        )
    }

}